/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>

declare var document: Document;

const test: HTMLButtonElement = <HTMLButtonElement> document.getElementById(
  "test",
);
const send: HTMLInputElement = <HTMLInputElement> document.getElementById(
  "send",
);
const messages: HTMLDivElement = <HTMLDivElement> document.getElementById(
  "messages",
);
const socket = new WebSocket(`ws://${window.location.host}/ws`);
socket.onmessage = async (event) => {
  let data;
  if (typeof event.data === "string") {
    data = "STRING:" + event.data;
  } else {
    data = await event.data.arrayBuffer();
    data = new Uint8Array(data);
    let x = 0;
    for (const i of data) {
      x += i;
    }
    data = x;
  }
  messages.innerHTML = data.toString() + "<br />" + messages.innerHTML;
};
send.onclick = () => {
  let length = parseInt(test.value);
  if (isNaN(length)) {
    messages.innerHTML = "NaN, NO." + "<br />" + messages.innerHTML;
    return;
  }
  const output: number[] = [];
  while (length > 0) {
    if (length >= 255) {
      output.push(255);
    } else {
      output.push(length);
    }
    length -= 255;
  }
  const returning = new Uint8Array(output);
  console.log("Sending" + returning);
  socket.send(returning.buffer);
};

// This is here b/c of Deno Issue #8684
export const sideEffectOnly = true;
