import { Application, Context } from "https://deno.land/x/oak@v6.3.2/mod.ts";
import {
  handler,
  WebSocketMiddleware,
} from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.0.1/mod.ts";
import {
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket,
} from "https://deno.land/std@0.77.0/ws/mod.ts";
// deno-lint-ignore camelcase
import { BCC_Middleware } from "https://raw.githubusercontent.com/jcc10/oak_bundle-compile-cache_middleware/v1.1.2/mod.ts";

const socketHandler: handler = async function (
  socket: WebSocket,
  url: URL,
): Promise<void> {
  // Wait for new messages
  try {
    for await (const ev of socket) {
      if (ev instanceof Uint8Array) {
        // binary message
        const view = new Uint8Array(ev);
        let length = view.length;
        const output = [];
        while (length > 0) {
          if (length >= 255) {
            output.push(255);
          } else {
            output.push(length);
          }
          length -= 255;
        }
        console.log(output);
        const returning = new Uint8Array(output);
        socket.send(returning);
      } else if (isWebSocketPingEvent(ev)) {
        const [, body] = ev;
        // ping
        // stub
        //console.log("ws:Ping", body);
      } else if (isWebSocketCloseEvent(ev)) {
        // close
        const { code, reason } = ev;
        //console.log("ws:Close", code, reason);
      }
    }
  } catch (err) {
    console.error(`failed to receive frame: ${err}`);

    if (!socket.isClosed) {
      await socket.close(99).catch(console.error);
    }
  }
};

const app = new Application();
app.use(WebSocketMiddleware(socketHandler));

const bccMiddle = new BCC_Middleware({
  BCC_Settings: {
    tsSource: "client-ts",
    bundleFolder: undefined,
    compiledFolder: "compiled",
    cacheFolder: undefined,
    cacheRoot: undefined,
    mapSources: false,
  },
});
// Clear old files.
await bccMiddle.bcc.clearAllCache();
await bccMiddle.bcc.compile("script.ts");
// Load the middleware.
app.use(bccMiddle.middleware());

app.use(async (ctx: Context) => {
  const decoder = new TextDecoder("utf-8");
  const bytes = await Deno.readFile("./client.html");
  const text = decoder.decode(bytes);
  ctx.response.body = text;
});
const appPromise = app.listen({ port: 3000 });
console.log("Server running on localhost:3000");
